var morgan = require('morgan');
var bodyParser = require('body-parser');
const helmet = require('helmet');
const compression = require('compression');
const isDev = process.env.NODE_ENV === 'development';
const isProd = process.env.NODE_ENV === 'production';

module.exports = (app) => {
    if (isProd) {
        app.use(compression());
        app.use(helmet());
    }
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    if (isDev) {
        app.use(morgan('dev'));
    }
}