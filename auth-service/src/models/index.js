"use strict";
var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
// var { fileURLToPath } = require('url');
// var { dirname } = require('path');

// eslint-disable-next-line no-unused-vars
var { config, configPostgres } = require('../config/index.js');

const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

// ------------Postgres------------
const pgConfig = configPostgres[process.env.NODE_ENV]
// const sequelize = new Sequelize(pgConfig.uri, { logging: pgConfig.logging }) //with URI
console.log({
    dialect: 'postgres',
    username: pgConfig.pgUser,
    password: pgConfig.pgPassword,
    database: pgConfig.pgDB,
    host: pgConfig.pgHost,
    port: pgConfig.pgPort,
    logging: pgConfig.logging
});
const sequelize = new Sequelize(
    {
        dialect: 'postgres',
        username: pgConfig.pgUser,
        password: pgConfig.pgPassword,
        database: pgConfig.pgDB,
        host: pgConfig.pgHost,
        port: pgConfig.pgPort,
        logging: pgConfig.logging
    }
);

// ------------Mysql------------
// const logging = config.nodeEnv === 'development' ? console.log : false;
// const sequelize = new Sequelize(
//     {
//         dialect: 'mysql',
//         username: config.mysqlUser,
//         password: config.mysqlPassword,
//         database: config.mysqlDB,
//         host: config.mysqlHost,
//         port: config.mysqlPort,
//         logging
//     }
// );


const db = fs
    .readdirSync(__dirname)
    .filter(filename => /model.js$/.test(filename))
    .reduce((total, filename) => {
        const model = require(path.join(__dirname, filename))(sequelize, Sequelize.DataTypes)
        total[capitalize(model.name)] = model; // eslint-disable-line
        return total;
    }, {});
/**
 * Sets up the associations for each model.
 * @param  {string} modelName
 */
Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});
db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db