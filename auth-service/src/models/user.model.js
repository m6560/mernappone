var bCrypt = require('bcrypt-nodejs');
/**
* Example model - create and export the database model for the example
* including all assosiations and classmethods assiciated with this model.
* @memberof  module:models/Example
* @param  {Object} sequelize description
* @param  {Object} DataTypes description
*/

function UserModel(sequelize, DataTypes) {
    var generateHash = (password) => bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
    // const Session = sequelize.define({
    //     refreshToken: {
    //         type: String,
    //         default: "",
    //     },
    // })
    const User = sequelize.define('user', {
        id: {
            type: DataTypes.UUID,
            unique: true,
            defaultValue: DataTypes.UUIDV1,
            primaryKey: true
        },
        username: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        password: {
            type: DataTypes.VIRTUAL,
            set(val) {
                this.setDataValue('password', val);
                this.setDataValue('passwordHash', generateHash(val));
            },
            validate: {
                isLongEnough(val) {
                    if (val.length < 3) {
                        throw new Error('Please choose a longer password');
                    }
                }
            }
        },
        passwordHash: DataTypes.STRING,
        authStrategy: {
            type: DataTypes.STRING,
            default: "local",
        },
        points: {
            type: DataTypes.INTEGER,
            default: 50,
        },
        // refreshToken: {
        //     type: [Session],
        //     get: function() {
        //         return JSON.parse(this.getDataValue('myArrayField'));
        //     }, 
        //     set: function(val) {
        //         return this.setDataValue('myArrayField', JSON.stringify(val));
        //     }
        // },
    },
        {
            indexes: [{
                unique: true,
                fields: ['username']
            }]
        }
    );
    //Remove refreshToken from the response
    // User.set("toJSON", {
    //     transform: function (doc, ret, options) {
    //         delete ret.refreshToken
    //         return ret
    //     },
    // })

    // Account.associate = (models) => {
    //     Account.roles = Account.hasMany(models.AccountRole, { as: 'roles' });
    //     // eslint-disable-next-line max-len
    //     Account.accountApiKey = Account.hasMany(models.AccountApiKey, { as: 'account_api_key' });
    // };
    return User;
}
module.exports = UserModel;