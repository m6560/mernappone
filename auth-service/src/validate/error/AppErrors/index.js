'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _extendableBuiltin3(cls) {
    function ExtendableBuiltin() {
        var instance = Reflect.construct(cls, Array.from(arguments));
        Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
        return instance;
    }

    ExtendableBuiltin.prototype = Object.create(cls.prototype, {
        constructor: {
            value: cls,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });

    if (Object.setPrototypeOf) {
        Object.setPrototypeOf(ExtendableBuiltin, cls);
    } else {
        ExtendableBuiltin.__proto__ = cls;
    }

    return ExtendableBuiltin;
}

function _extendableBuiltin2(cls) {
    function ExtendableBuiltin() {
        var instance = Reflect.construct(cls, Array.from(arguments));
        Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
        return instance;
    }

    ExtendableBuiltin.prototype = Object.create(cls.prototype, {
        constructor: {
            value: cls,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });

    if (Object.setPrototypeOf) {
        Object.setPrototypeOf(ExtendableBuiltin, cls);
    } else {
        ExtendableBuiltin.__proto__ = cls;
    }

    return ExtendableBuiltin;
}

function _extendableBuiltin(cls) {
    function ExtendableBuiltin() {
        var instance = Reflect.construct(cls, Array.from(arguments));
        Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
        return instance;
    }

    ExtendableBuiltin.prototype = Object.create(cls.prototype, {
        constructor: {
            value: cls,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });

    if (Object.setPrototypeOf) {
        Object.setPrototypeOf(ExtendableBuiltin, cls);
    } else {
        ExtendableBuiltin.__proto__ = cls;
    }

    return ExtendableBuiltin;
}

class AppError extends _extendableBuiltin(Error) {
    constructor(message = 'Internal Service Error') {
        super(message);
        this.name = this.constructor.name;
        this.status = 500;
        Error.captureStackTrace(this, AppError);
    }
}

exports.AppError = AppError;
class RedisError extends _extendableBuiltin2(Error) {
    constructor(errorCode = '') {
        let message = 'Redis Error Message';
        if (errorCode === 'NR_CLOSED') {
            message = `The connection is already closed with error code: ${errorCode}`;
        }
        if (errorCode === 'UNCERTAIN_STATE') {
            message = `A command unresolved with error code: ${errorCode}`;
        }
        if (errorCode === 'CONNECTION_BROKEN') {
            message = `node_redis gives up to reconnect with error code: ${errorCode}`;
        }
        super(message);
        this.name = this.constructor.name;
        this.status = 500;
        Error.captureStackTrace(this, RedisError);
    }
}

exports.RedisError = RedisError;
class BillServiceError extends AppError {
    constructor(message = 'Bill Error.') {
        super(message);
        this.message = message;
        this.name = 'BillServiceError';
    }
}

exports.BillServiceError = BillServiceError;
class CronJobError extends AppError {
    constructor(error) {
        super(error.message);
        this.name = 'CronJobError';
        this.status = 500;
    }
}

exports.CronJobError = CronJobError;
class ExternalServiceError extends AppError {
    constructor(error) {
        super();
        if (error instanceof Error) {
            const { status, headers, config: configuration, data } = error.response;
            this.response = { status, headers, configuration, data };
        }
        this.name = this.constructor.name;
    }
}

exports.ExternalServiceError = ExternalServiceError;
class RequestExternalServiceError extends AppError {
    constructor(error) {
        super(error.message);
        this.name = this.constructor.name;
        this.extServiceRequest = error.request;
    }
}

exports.RequestExternalServiceError = RequestExternalServiceError;
class StaffServiceError extends ExternalServiceError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}

exports.StaffServiceError = StaffServiceError;
class ExtUserServiceError extends ExternalServiceError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}

exports.ExtUserServiceError = ExtUserServiceError;
class PolicyServiceError extends ExternalServiceError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}

exports.PolicyServiceError = PolicyServiceError;
class FileCreatorError extends ExternalServiceError {
    constructor(error) {
        super(error);
        if (!(error instanceof Error)) {
            this.response = error;
        }
        this.name = this.constructor.name;
    }
}

exports.FileCreatorError = FileCreatorError;
class TemplateNotExistError extends FileCreatorError {
    constructor(error) {
        super(error);
        this.message = 'The template not exist or not active. Please contact with UW Admin and Company Admin'; // eslint-disable-line
        this.name = this.constructor.name;
        this.status = 400;
    }
}

exports.TemplateNotExistError = TemplateNotExistError;
class ApiKeyInvalidError extends FileCreatorError {
    constructor(error) {
        super(error);
        this.message = 'The api key for cetificate invalid. Please contact with UW admin and Company Admin'; // eslint-disable-line
        this.name = this.constructor.name;
        this.status = 400;
    }
}

exports.ApiKeyInvalidError = ApiKeyInvalidError;
class SecretKeyInvalidError extends FileCreatorError {
    constructor(error) {
        super(error);
        this.message = 'The secret key for cetificate invalid. Please contact with UW admin and Company Admin'; // eslint-disable-line
        this.name = this.constructor.name;
        this.status = 400;
    }
}
exports.SecretKeyInvalidError = SecretKeyInvalidError;
class ProductServiceError extends ExternalServiceError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}

exports.ProductServiceError = ProductServiceError;
class SubValidationError extends _extendableBuiltin3(Error) {
    constructor(error, Sequelize) {
        super('ValidationError');
        this.name = 'ValidationError';
        this.status = 400;
        const { subName = '' } = error;
        // this.errors = error.errors
        if (error instanceof Sequelize.ValidationError) {
            this.errors = error.errors.reduce((finalErrors, itemError) => _extends({}, finalErrors, {
                [subName + itemError.path]: {
                    key: itemError.validatorKey,
                    agrs: itemError.validatorArgs
                }
            }), {});
        }
    }
}
exports.SubValidationError = SubValidationError;