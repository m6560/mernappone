/**
 * Main config file used throughout the application
 * @module config
 * @type {object}
 * @property {integer} port - The port to expose to application to.
 * @property {string} pgUrl - The url used by sequlize to connect to postgres.
 * @property {string} nodeEnv - The environment to run the server in
 * @property {string} sentry - Sentry DSN

 */


// const file = path.resolve(__dirname, '../assets/keys/public_key.pem');

const config = {
    port: process.env.PORT || 3001,
    mysqlHost: process.env.PG_HOST || 'localhost',
    mysqlPort: process.env.PG_PORT || 3307,
    mysqlUser: process.env.PG_USER || 'root',
    mysqlPassword: process.env.PG_PASSWORD || '123123',
    mysqlDB: process.env.PG_DB || 'mern',
    nodeEnv: process.env.NODE_ENV || 'development',
    sentry: process.env.SENTRY_DSN || '',
    jwtSecrect: process.env.JWT_SECRET || 'sml#authen',
    encryptKey: process.env.ENCRYPT_KEY || 'sm-encrypt-secret-key',
    logLevel: process.env.LOG_LEVEL || 'info',
    enableCORS: process.env.ENABLE_CORS || undefined,
};

const configPostgres = {
    development: {
        port: process.env.PORT || 3001,
        pgHost: process.env.PG_HOST || 'db',
        pgPort: process.env.PG_PORT || 5432,
        pgUser: process.env.PG_USER || 'postgres',
        pgPassword: process.env.PG_PASSWORD || 'password',
        pgDB: process.env.PG_DB || 'postgres',
        pglogging: true
    }
};

// const configsPG = {
//     development: {
//         uri: process.env?.DEV_DB_URI ?? 'postgres://postgres:password@localhost:5432/service_auth',
//         logging: true
//     },
//     test: {
//         uri: process.env?.TEST_DB_URI ?? 'sqlite::memory:',
//         logging: false
//     },
//     production: {
//         uri: process.env?.PROD_DB_URI ?? 'postgres://postgres:password@localhost:5432/service_auth',
//         logging: false
//     }
// }

module.exports = { config, configPostgres };
