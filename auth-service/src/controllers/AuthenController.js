const getHome = (req, res) => {
    res.render('index', { user: req.user });
}
const getDashboard = (req, res) => {
    res.render('dashboard');
}
const getRegister = (req, res) => {
    res.render('register', {});
}
const getLogin = (req, res) => {
    res.render('login', { user: req.user });
}
const postLogin = (req, res) => {
    res.redirect('/');
}
const getLogout = (req, res) => {
    req.logout();
    req.session.destroy(() => {
        res.status(200);
    });
}
const isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) return next();
    res.redirect('/login');
}
module.exports = {
    isLoggedIn,
    getHome,
    getRegister,
    getLogin,
    postLogin,
    getLogout,
    getDashboard
};