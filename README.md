# The service is built on the top of ExpressJS and Sequelize is used as ORM. While PostgreSQL is used as prod database, SQLite is preferred as test database.
# Node target version is 16. 

# Currently, only the database config is determined by this parameter. It takes the following values.
production
development
test
--------How to create models-----------
# Create model : example model "User"
npx sequelize-cli model:generate --name User --attributes firstName:string,lastName:string,email:string
# Execute migration
npx sequelize-cli db:migrate

-------------How to add default data---------------------
# Create seed : example model "demo-user"
npx sequelize-cli seed:generate --name demo-user 
# Running seed
npx sequelize-cli db:seed:all

------------Run-Docker---------------------
#  cd /dev-env
# docker-compose up -d
 Sử dụng PostMan:
#   1.Create user:
    - http://localhost:80/auth/register
    - body: {
        "username": "testuser",
        "password": "123123"
    }
#   2.Login user:
    -http://localhost:80/auth/login
    -body: {
        "username": "testuser",
        "password": "123123"
    }
#  lấy danh sách sản phẩm
    http://localhost:80/product/10
    (nếu đã đăng nhập sẽ được trả về array product, ngược lại sẽ nhận unAuthen (bị chặn ở gateway))



