var axios = require("axios");

const axiosInstance = axios.create({
    baseURL: 'http://localhost:3001/auth',
    timeout: 1000,
    headers: { 'X-Custom-Header': 'foobar' }
});

const checkJWT = async (req, res, next) => {
    try {
        const token = req.headers.authorization;
        axiosInstance.defaults.headers.Authorization = token;
        const user = await axiosInstance.get('/checkAuthen');
        console.log(user);
        next();
    } catch (error) {
        res.status(401).send(error)
    }
}
module.exports = checkJWT;