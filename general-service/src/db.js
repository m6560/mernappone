const { migrateDB } = require('firis-service-sdk-test2/db-helpers');
const models = require('./models');

const pathToMigration = `${__dirname}/migrations/`;
const db = migrateDB(models, pathToMigration);

module.exports = db;
