var express = require('express');
var router = express.Router();
var auth = require('./auth');
var product = require('./product');

router.use('/auth', auth);
router.use('/product', product);

module.exports = router;