var express = require('express');
const Joi = require('joi');
var passport = require('passport');
var router = express.Router();
var { getHome, getRegister, getDashboard, getLogin, getLogout, isLoggedIn } = require('../../controllers/Auth/AuthenController');
const { validateRequest } = require('../../validate/auth.validate');

router.get('/', getHome);
router.get('/dashboard', isLoggedIn, getDashboard);
router.get('/register', getRegister);
router.get('/login', getLogin);

router.get('/checkAuthen', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.setHeader('User-Id', req.user.id)
    res.sendStatus(200)
});

router.post('/register', passport.authenticate('local-signup', { session: false }), (req, res) => {
    res.status(200).send({ isSuccess: true, info: req.user })
});

router.post('/login', validateRequest({
    bodySchema: Joi.object().keys({
        username: Joi.string().min(5).required(),
        password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required()
    })
}), passport.authenticate('local-signin', { session: false }), (req, res) => {
    res.status(200).json({ isSuccess: true, token: req.user })
});

router.get('/logout', getLogout);

router.get('/ping', function (req, res) {
    res.status(200).send(`pon g! `);
});

module.exports = router;