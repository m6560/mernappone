var express = require('express');
var router = express.Router();
const { getProduct, createProduct, updateProduct, deleteProduct } = require('../../controllers/Product/ProductsController');
// const checkJWT = require('../middleware/authen');

/* GET users listing. */
router.get('/:top', getProduct);
router.post('/', createProduct);
router.patch('/:id', updateProduct);
router.delete('/:id', deleteProduct);

module.exports = router;
