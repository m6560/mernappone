//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
// eslint-disable-next-line no-unused-vars
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('Post', () => {
    beforeEach((done) => {
        //Before each test we empty the database in your case
        done();
    });
    /*
     * Test the /GET route
     */
    describe('/GET post', () => {
        it('It should GET all the post', (done) => {
            chai.request(server)
                .get('/posts/25')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(25); // fixme :)
                    done();
                });
        });
    });
});