var db = require('../../models');
// const redis = require('redis');

// // create and connect redis client to local instance.
// const client = redis.createClient(6379);
// // echo redis errors to the console
// client.on('error', (err) => { console.log("Error " + err) });

// const getPost = (req, res) => {
//     // key to store results in Redis store
//     const postsRedisKey = 'user:posts';
//     return client.get(postsRedisKey, (err, posts) => {
//         // If that key exists in Redis store
//         if (posts) {
//             return res.status(200).send({ source: 'cache', data: JSON.parse(posts) })
//         } else { // Key does not exist in Redis store
//             // Fetch directly from remote api
//             db.Product.findAll().then(response => response.json())
//                 .then(product => {
//                     // Save the  API response in Redis store,  data expire time in 3600 seconds, it means one hour
//                     client.setex(postsRedisKey, 3600, JSON.stringify(product))
//                     // Send JSON response to client
//                     return res.status(200).send({ source: 'api', data: product })
//                 }).catch(error => {
//                     // log error message
//                     console.log(error)
//                     // send error to the client 
//                     return res.status(500).send({
//                         message: err || "Error"
//                     });
//                 })
//         }
//     })
// }

const getProduct = (req, res) => {
    db.Product.findAll({
        limit: parseInt(req.params.top),
    }).then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.status(500).send({
            message: err || "Error"
        });
    });
}
const createProduct = (req, res) => {
    const id = req.params.id;
    db.Product.update(req.body, { where: { id: id } })
        .then(num => {
            if (num == 1) {
                res.send({ message: "Product was updated successfully." });
            } else {
                res.send({
                    message: `Cannot update Product with id=${id}. Maybe Tutorial was not found or req.body is empty!`
                });
            }
        }).catch(err => {
            res.status(500).send({
                message: err || "Error updating Tutorial with id=" + id
            });
        });
}
const updateProduct = (req, res) => {
    const id = req.params.id;
    db.Product.destroy({
        where: { id: id }
    }).then(num => {
        if (num == 1) {
            res.send({
                message: "Product was deleted successfully!"
            });
        } else {
            res.send({
                message: `Cannot delete Product with id=${id}. Maybe Tutorial was not found!`
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: err || "Could not delete Tutorial with id=" + id
        });
    });
}
const deleteProduct = (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }
    // Create a Product
    const product = {
        title: req.body.title,
        content: req.body.content,
    };
    // Save Product in the database
    db.Product.create(product).then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occurred while creating the Product"
        });
    });
}
module.exports = {
    getProduct,
    createProduct,
    updateProduct,
    deleteProduct
};