'use strict';
var faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {
    const creatDataFake = Array.from(Array(1000).keys()).map(cur => ({
      id: faker.random.uuid(),
      title: faker.name.title(),
      content: faker.name.jobDescriptor(),
      createdAt: new Date(),
      updatedAt: new Date()
    }));
    return queryInterface.bulkInsert('products', creatDataFake);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('products', null, {});
  }
};
