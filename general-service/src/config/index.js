/**
 * Main config file used throughout the application
 * @module config
 * @type {object}
 * @property {integer} port - The port to expose to application to.
 * @property {string} pgUrl - The url used by sequlize to connect to postgres.
 * @property {string} nodeEnv - The environment to run the server in
 * @property {string} sentry - Sentry DSN

 */


// const file = path.resolve(__dirname, '../assets/keys/public_key.pem');

const config = {
    port: process.env.PORT || 3001,
    mysqlHost: process.env.DB_HOST || 'localhost',
    mysqlPort: process.env.MYSQL_PORT || 3306,
    mysqlUser: process.env.MYSQL_USER || 'root',
    mysqlPassword: process.env.MYSQL_PASSWORD || '123123',
    mysqlDB: process.env.MYSQL_DB || 'mern',
    nodeEnv: process.env.NODE_ENV || 'development',
    sentry: process.env.SENTRY_DSN || '',
    jwtSecrect: process.env.JWT_KEY || 'sml#authen',
    encryptKey: process.env.ENCRYPT_KEY || 'sm-encrypt-secret-key',
    logLevel: process.env.LOG_LEVEL || 'info',
    accessTokenExpiration: process.env.ACCESS_TOKEN_EXPIRATION || '900000', // unit: millisecond, 15 minutes https://www.npmjs.com/package/jsonwebtoken
    refreshTokenExpiration: process.env.REFRESH_TOKEN_EXPIRATION || '1296000000', // unit: millisecond ,15 days https://www.npmjs.com/package/jsonwebtoken
    internalTokenExpiration: process.env.INTERNAL_TOKEN_EXPIRATION || '300000', // 5 minutes
    redisHost: process.env.REDIS_HOST || 'localhost',
    redisPort: process.env.REDIS_PORT || '6379',
    redisPass: process.env.REDIS_PASSWORD || '',
    algorithmEncrypt: process.env.algorithmEncryp || 'aes-256-cbc',
    enableCORS: process.env.ENABLE_CORS || undefined,
};
const configPostgres = {
    development: {
        port: process.env.PORT || 3001,
        pgHost: process.env.DB_HOST || 'db',
        pgPort: process.env.PG_PORT || 5432,
        pgUser: process.env.PG_USER || 'postgres',
        pgPassword: process.env.PG_PASSWORD || 'password',
        pgDB: process.env.PG_DB || 'postgres',
        pglogging: true
    }
};
module.exports = { config, configPostgres };
