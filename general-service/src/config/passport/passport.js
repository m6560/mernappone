// var sha256 = require('sha256');
var bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var LocalStrategy = require('passport-local').Strategy;
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var config = require('../index');
var opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwtSecrect || 'secret'
}
// opts.issuer = 'accounts.examplesoft.com';
// opts.audience = 'yoursite.net';

function Passport(passport, User) {
    // serialize 
    passport.serializeUser((user, done) => { done(null, user.id); });
    // deserialize user 
    passport.deserializeUser((id, done) => {
        User.findByPk(id).then(function (user) {
            if (user) { done(null, user.get()); }
            else { done(user.errors, null); }
        });
    });

    //Check JWT 
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        User.findOne({ where: { id: jwt_payload.id } }).then((user) => {
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
                // or you could create a new account
            }
        }).catch(err => done(err, false));
    }));

    //LOCAL SIGNIN 
    passport.use('local-signin', new LocalStrategy(
        {  // by default, local strategy uses username and password, we will override with email 
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
            // allows us to pass back the entire request to the callback 
        }, (req, username, password, done) => {
            var isValidPassword = (userpass, password) => bCrypt.compareSync(password, userpass);
            User.findOne({ where: { username: username } })
                .then((user) => {
                    if (!user) {
                        return done(true, false, { message: 'username does not exist' });
                    }
                    if (!isValidPassword(user.passwordHash, password)) {
                        return done(true, true, { message: 'Incorrect password.' });
                    }
                    var userinfo = user.get();
                    //jwt sign
                    delete userinfo['passwordHash'];
                    const token = jwt.sign(userinfo, opts.secretOrKey);
                    return done(null, token);
                }).catch(() => {
                    return done(true, false, { message: 'Something went wrong with your Signin' });
                });
        })
    );
    //LOCAL SIGNUP
    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback 
        }, (req, username, password, done) => {
            User.findOne({ where: { username: username } }).then(resUser => {
                if (resUser) {
                    return done(null, false, { message: 'That username is already taken' });

                } else {
                    var data = { username, password };
                    User.create(data).then((newUser) => {
                        if (!newUser) { return done(null, false); }
                        if (newUser) {
                            const user = newUser.get();
                            delete user['password'];
                            delete user['passwordHash'];
                            return done(null, user);
                        }
                    })
                }
            });

        }
    ));

}
module.exports = Passport