/**
* Example model - create and export the database model for the example
* including all assosiations and classmethods assiciated with this model.
* @memberof  module:models/Example
* @param  {Object} sequelize description
* @param  {Object} DataTypes description
*/

function ProductModel(sequelize, DataTypes) {
    const Product = sequelize.define('product', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        title: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        content: {
            type: DataTypes.STRING(1000),
            allowNull: false,
        },
        // It is possible to create foreign keys:
        authorId: {
            type: DataTypes.UUID,
            allowNull: true,
            references: {
                model: 'authors',
                key: 'id'
            }
        },
    }, {
        timestamps: true,
    });

    // Account.associate = (models) => {
    //     Account.roles = Account.hasMany(models.AccountRole, { as: 'roles' });
    //     // eslint-disable-next-line max-len
    //     Account.accountApiKey = Account.hasMany(models.AccountApiKey, { as: 'account_api_key' });
    // };
    return Product;
}
module.exports = ProductModel;