'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const { config, configPostgres } = require('../config/index.js');
const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);
const logging = config.nodeEnv === 'development' ? console.log : false;

// ----Postgres---
const pgConfig = configPostgres[process.env.NODE_ENV];
console.log(pgConfig);
let sequelize = new Sequelize(
  {
    dialect: 'postgres',
    username: pgConfig.pgUser,
    password: pgConfig.pgPassword,
    database: pgConfig.pgDB,
    host: pgConfig.pgHost,
    port: pgConfig.pgPort,
    logging
  }
);

// --- Mysql-- -
// let sequelize = new Sequelize(
//   {
//     dialect: 'mysql',
//     username: config.mysqlUser,
//     password: config.mysqlPassword,
//     database: config.mysqlDB,
//     host: config.mysqlHost,
//     port: config.mysqlPort,
//     logging
//   }
// );

const db = fs
  .readdirSync(__dirname)
  .filter(filename => /model.js$/.test(filename))
  .reduce((total, filename) => {
    const model = require(path.join(__dirname, filename))(sequelize, Sequelize.DataTypes)
    total[capitalize(model.name)] = model;
    return total;
  }, {});

/**
 * Sets up the associations for each model.
 * @param  {string} modelName
 */
Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
