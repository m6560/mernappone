/**
* Example model - create and export the database model for the example
* including all assosiations and classmethods assiciated with this model.
* @memberof  module:models/Example
* @param  {Object} sequelize description
* @param  {Object} DataTypes description
*/

function AuthorModel(sequelize, DataTypes) {
    const Author = sequelize.define('authors', {
        id: {
            type: DataTypes.UUID,
            allowNull: false,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true
        },
        name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        age: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        email: {
            type: DataTypes.TEXT,
            allowNull: true
        }
    }, {});

    // Account.associate = (models) => {
    //     Account.roles = Account.hasMany(models.AccountRole, { as: 'roles' });
    //     // eslint-disable-next-line max-len
    //     Account.accountApiKey = Account.hasMany(models.AccountApiKey, { as: 'account_api_key' });
    // };
    return Author;
}
module.exports = AuthorModel;