'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.UnSubcribleError = exports.PolicyEffectiveDateAfterInsuredMemberEffectiveDateError = exports.EndorsementExpiryDateBeforeInsuredMemberExpiryDateError = exports.EndorsementEffectiveDateAfterInsuredMemberEffectiveDateError = exports.PolicyExpiredDateBeforeInsuredMemberExpiryDateError = exports.InsurMemberDuplicateError = exports.DeductibleError = exports.ProductPlanError = exports.BenefitRateError = exports.PolicyPlanNotFound = exports.PolicyError = exports.InsurMemberExpiryDateError = exports.InsurMemberEffectiveDateError = exports.MissingSettingParamError = exports.SumAssuredError = exports.CarValueError = exports.AgeCarEligibilityError = exports.AgeEligibilityError = exports.PremiumEligibilityError = exports.DatabaseError = exports.UriValidationError = exports.ActionError = exports.AuthorizationError = exports.TokenError = exports.AuthenticationError = exports.ResourceNotFoundError = exports.RequestBodyValidationError = exports.RequestQueryValidationError = exports.RequestParamValidationError = exports.RequestValidationError = exports.CustomValidationError = exports.DataValidationError = exports.APINotFoundError = exports.BusinessError = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _AppErrors = require('../AppErrors');

class BusinessError extends _AppErrors.AppError {
    constructor(message = 'There is business error happened') {
        super(message);
        this.name = 'BusinessError';
        this.status = 400;
    }
}

exports.BusinessError = BusinessError; /**
                                       * pageNotFoundMiddleware - Returns a 404 Page not Found error it the route requested
                                       * is not matched with any corresponding route.
                                       *
                                       * @param  {Object} req - Express request object
                                       * @param  {Object} res - Express response object
                                       * @return {Undefined}  - A 404 not found error
                                       */

class APINotFoundError extends BusinessError {
    constructor(message = 'API not found') {
        super(message);
        this.name = 'APINotFoundError';
        this.status = 404;
    }
}

exports.APINotFoundError = APINotFoundError; /**
                                             * ValidationError - Returns an Error message for Sequelize validation with
                                             * information about what's wrong
                                             *
                                             * @param  {Object} error - Validation error from  Sequelize
                                             * @return {Object}  - A ValidationError object
                                             */

class DataValidationError extends BusinessError {
    constructor(err, sequelize) {
        super(err.message);
        this.name = 'DataValidationError';
        this.status = 400;
        if (err instanceof sequelize.ValidationError) {
            this.payload = err.errors.reduce((finalErrors, itemError) => _extends({}, finalErrors, {
                [itemError.path]: {
                    message: itemError.message,
                    type: itemError.validatorKey,
                    context: {
                        value: itemError.value,
                        agrs: itemError.validatorArgs
                    }
                }
            }), {});
        }
    }
}

exports.DataValidationError = DataValidationError; /**
                                                   * CustomValidationError - Returns an Error message for general validation with
                                                   * message about what's wrong
                                                   *
                                                   * @param  {Object} error - Validation error from  Sequelize
                                                   * @return {Object}  - A ValidationError object
                                                   */

class CustomValidationError extends BusinessError {
    constructor(message = 'Bad request.') {
        super(message);
        this.message = message;
        this.name = 'CustomValidationError';
        this.status = 400;
    }
}

exports.CustomValidationError = CustomValidationError;
class RequestValidationError extends BusinessError {
    constructor(err) {
        super(err.message);
        this.name = 'RequestValidationError';
        this.status = 400;
        this.payload = err.details.reduce((finalErrors, { path, message, context, type }) => _extends({}, finalErrors, { [path]: { message, type, context } }), {});
    }
}

exports.RequestValidationError = RequestValidationError;
class RequestParamValidationError extends RequestValidationError {
    constructor(err) {
        super(err);
        this.name = 'RequestParamValidationError';
    }
}

exports.RequestParamValidationError = RequestParamValidationError;
class RequestQueryValidationError extends RequestValidationError {
    constructor(err) {
        super(err);
        this.name = 'RequestQueryValidationError';
    }
}

exports.RequestQueryValidationError = RequestQueryValidationError;
class RequestBodyValidationError extends RequestValidationError {
    constructor(err) {
        super(err);
        this.name = 'RequestBodyValidationError';
    }
}

exports.RequestBodyValidationError = RequestBodyValidationError; /**
                                                                 * ResourceNotFoundError - Returns a 404 Entity not found error for when
                                                                 * a client request an entity that isn't there
                                                                 *
                                                                 * @param  {String} entityType - The type of entity that isn't found
                                                                 * @return {Object}  - Error object
                                                                 */

class ResourceNotFoundError extends BusinessError {
    constructor(entityType = 'entity') {
        super(`Could not find ${entityType}`);
        this.name = 'ResourceNotFoundError';
        this.status = 404;
    }
}

exports.ResourceNotFoundError = ResourceNotFoundError; /**
                                                       * AuthenticationError - Returns a 401 You need to authenicate to access this resource,
                                                       * implying that the user does not have enough premissions to complete this request.
                                                       *
                                                       * @param  {String} message - A message sent to the user why he/she is not authenticated
                                                       * @return {Object}  - Error object
                                                       */

class AuthenticationError extends BusinessError {
    constructor(message = 'You need to authenicate to access this resource') {
        super(message);
        this.name = 'AuthenticationError';
        this.status = 401;
    }
}
exports.AuthenticationError = AuthenticationError; /**
                                                   * AuthorizationError - Returns a 400 You send invalid token to access this resource,
                                                   * implying that the user sends invalid token to complete this request.
                                                   *
                                                   * @param  {String} message - A message sent to the user why he/she is not authenticated
                                                   * @return {Object}  - Error object
                                                   */

class TokenError extends BusinessError {
    constructor(message = 'You need to provide valid token to access this resource') {
        super(message);
        this.name = 'TokenError';
        this.status = 401;
    }
}

exports.TokenError = TokenError; /**
                                 * AuthorizationError - Returns a 401 You are not authorized to access this resource,
                                 * implying that the user does not have enough premissions to complete this request.
                                 *
                                 * @param  {String} message - A message sent to the user why he/she is not authenticated
                                 * @return {Object}  - Error object
                                 */

class AuthorizationError extends BusinessError {
    constructor(message = 'You are not authorized to access this resource') {
        super(message);
        this.name = 'AuthorizationError';
        this.status = 403;
    }
}

exports.AuthorizationError = AuthorizationError; /**
                                                 * All error handeling and errors used trouought the application.
                                                 * @module components/errors
                                                 */

/**
 * ActionError - Returns a 400 Invalid Action.
 *
 * @return {Object}  - Error object
 */

class ActionError extends BusinessError {
    constructor(message = 'Invalid Action') {
        super(message);
        this.message = message;
        this.name = 'ActionError';
        this.status = 400;
    }
}

exports.ActionError = ActionError; /**
                                    * UriValidationError - Returns a 400 Invalid URI.
                                    * indicating that the URI the user gave was invalid.
                                    *
                                    * @return {Object}  - Error object
                                    */

class UriValidationError extends BusinessError {
    constructor(message = 'Invalid URI.') {
        super(message);
        this.message = message;
        this.name = 'UriValidationError';
        this.status = 400;
    }
}

exports.UriValidationError = UriValidationError; /**
                                                 * DatabaseError - Returns an Error message for Sequelize Database error.
                                                 * This is thrown in cases where validation in the DBMS is what stops the
                                                 * databse action, not Sequelize validation.
                                                 *
                                                 * @param  {Object} error - Database Error from Sequelize
                                                 * @return {Object}  - A DatabaseError object
                                                 */

class DatabaseError extends BusinessError {
    constructor(error) {
        super(error.message);
        this.name = 'DatabaseError';
        this.status = 400;
    }
}

exports.DatabaseError = DatabaseError;
class PremiumEligibilityError extends BusinessError {
    constructor(message = 'Premium Eligibility Error.') {
        super(message);
        this.message = message;
        this.name = 'PremiumEligibilityError';
    }
}
exports.PremiumEligibilityError = PremiumEligibilityError;
class AgeEligibilityError extends BusinessError {
    constructor(message = 'Age Eligibility Error.') {
        super(message);
        this.message = message;
        this.name = 'AgeEligibilityError';
    }
}

exports.AgeEligibilityError = AgeEligibilityError;
class AgeCarEligibilityError extends BusinessError {
    constructor(message = 'Age Car Eligibility Error.') {
        super(message);
        this.message = message;
        this.name = 'AgeCarEligibilityError';
    }
}

exports.AgeCarEligibilityError = AgeCarEligibilityError;
class CarValueError extends BusinessError {
    constructor(message = 'Car Value Error.') {
        super(message);
        this.message = message;
        this.name = 'CarValueError';
    }
}

exports.CarValueError = CarValueError;
class SumAssuredError extends BusinessError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}

exports.SumAssuredError = SumAssuredError;
class MissingSettingParamError extends BusinessError {
    constructor() {
        super();
        this.message = 'There are some missing settings to create certificate. Please contact with UW Admin and Company Admin'; // eslint-disable-line
        this.name = this.constructor.name;
        this.status = 400;
    }
}

exports.MissingSettingParamError = MissingSettingParamError;
class InsurMemberEffectiveDateError extends BusinessError {
    constructor(message = 'Insurance Member Effective Date Error.') {
        super(message);
        this.message = message;
        this.name = 'InsurMemberEffectiveDateError';
    }
}

exports.InsurMemberEffectiveDateError = InsurMemberEffectiveDateError;
class InsurMemberExpiryDateError extends BusinessError {
    constructor(message = 'Insurance Member Expiry Date Error.') {
        super(message);
        this.message = message;
        this.name = 'InsurMemberExpiryDateError';
    }
}

exports.InsurMemberExpiryDateError = InsurMemberExpiryDateError;
class PolicyError extends BusinessError {
    constructor(message = 'Policy Error.') {
        super(message);
        this.message = message;
        this.name = 'PolicyError';
    }
}
exports.PolicyError = PolicyError;
class PolicyPlanNotFound extends BusinessError {
    constructor(message = 'Policy plan not found.') {
        super(message);
        this.message = message;
        this.name = 'PolicyPlanNotFound';
    }
}

exports.PolicyPlanNotFound = PolicyPlanNotFound;
class BenefitRateError extends BusinessError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}

exports.BenefitRateError = BenefitRateError;
class ProductPlanError extends BusinessError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}

exports.ProductPlanError = ProductPlanError;
class DeductibleError extends BusinessError {
    constructor(message = 'Deductible Error') {
        super(message);
        this.message = message;
        this.name = 'DeductibleError';
    }
}

exports.DeductibleError = DeductibleError;
class InsurMemberDuplicateError extends BusinessError {
    constructor(message = 'InsurMember Duplicate Error') {
        super(message);
        this.message = message;
        this.name = 'InsurMember Duplicate Error';
    }
}

exports.InsurMemberDuplicateError = InsurMemberDuplicateError;
class PolicyExpiredDateBeforeInsuredMemberExpiryDateError extends BusinessError {

    /**
     * @param {Object} error
     * @param {string} error.message
     * @param {Object} error.payload
     */
    constructor(error) {
        super(error.message);
        this.name = 'PolicyExpiredDateBeforeInsuredMemberExpiryDateError';
        this.payload = error.payload;
    }
}

exports.PolicyExpiredDateBeforeInsuredMemberExpiryDateError = PolicyExpiredDateBeforeInsuredMemberExpiryDateError;
class EndorsementEffectiveDateAfterInsuredMemberEffectiveDateError extends BusinessError {

    /**
     * @param {Object} error
     * @param {string} error.message
     * @param {Object} error.payload
     */
    constructor(error) {
        super(error.message);
        this.name = 'EndorsementEffectiveDateAfterInsuredMemberEffectiveDateError';
        this.payload = error.payload;
    }
}

exports.EndorsementEffectiveDateAfterInsuredMemberEffectiveDateError = EndorsementEffectiveDateAfterInsuredMemberEffectiveDateError;
class EndorsementExpiryDateBeforeInsuredMemberExpiryDateError extends BusinessError {

    /**
     * @param {Object} error
     * @param {string} error.message
     * @param {Object} error.payload
     */
    constructor(error) {
        super(error.message);
        this.name = 'EndorsementExpiryDateBeforeInsuredMemberExpiryDateError';
        this.payload = error.payload;
    }
}

exports.EndorsementExpiryDateBeforeInsuredMemberExpiryDateError = EndorsementExpiryDateBeforeInsuredMemberExpiryDateError;
class PolicyEffectiveDateAfterInsuredMemberEffectiveDateError extends BusinessError {

    /**
     * @param {Object} error
     * @param {string} error.message
     * @param {Object} error.payload
     */
    constructor(error) {
        super(error.message);
        this.name = 'PolicyEffectiveDateAfterInsuredMemberEffectiveDateError';
        this.payload = error.payload;
    }
}

exports.PolicyEffectiveDateAfterInsuredMemberEffectiveDateError = PolicyEffectiveDateAfterInsuredMemberEffectiveDateError;
class UnSubcribleError extends BusinessError {
    constructor(error) {
        super(error);
        this.name = this.constructor.name;
    }
}
exports.UnSubcribleError = UnSubcribleError;