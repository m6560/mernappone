var _BusinessErrors = require('./error/BusinessErrors')
function validateRequest({ paramSchema, querySchema, bodySchema }) {
    return async (req, res, next) => {
        try {
            if (paramSchema) await paramSchema.validateAsync(req.body || {});
            if (querySchema) await querySchema.validateAsync(req.body || {});
            if (bodySchema) await bodySchema.validateAsync(req.body || {});
            return next();
        } catch (error) {
            return next(new _BusinessErrors.RequestBodyValidationError(error))
        }
    }
}
module.exports = { validateRequest };